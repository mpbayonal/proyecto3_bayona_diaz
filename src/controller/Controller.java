package controller;

import java.util.ArrayList;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.logic.VOStop;
import model.vo.Camino;
import model.vo.VOHoraParadaPorParada;
import model.vo.VOParada;
import model.vo.VOParadaCamino;
import model.vo.VOParadaCongestionada;
import model.vo.VOStopTime;



public class Controller 
{
	/**
	 * modela el manejador de la clase lógica
	 */
	private static ISTSManager manager  = new STSManager();

	public static void ITSInit() 
	{
		manager.ITSInit();
	}

	 
	public static void ITScargarGTFS() throws Exception 
	{
		manager.ITScargarGTFS();	
	}

	 	
	public static IList<Camino> darParadasAlcanzables(String stopId, String fecha) {
		return manager.darParadasAlcanzables(stopId, fecha);
	}

	
	public static IList<IList<VOParada>> darComponentesConexas() {
		return manager.darComponentesConexas();
	}

	
	public static void crearSubGrafo(String stopId, String horaInicio, String horaFin) {
		manager.crearSubGrafo(stopId, horaInicio, horaFin);
	}

	
	public static IList<VOStop> darParadasSubGrafo() {
		try {
			return manager.darParadasSubGrafo();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	
	public static IList<VOHoraParadaPorParada> darItinerarioLlegada(String stopId) {
		try {
			return manager.darItinerarioLlegada(stopId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	
	public static IList<VOHoraParadaPorParada> darItinerarioSalida(String stopId) {
		try {
			return manager.darItinerarioSalida(stopId);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	
	public static VOParadaCongestionada darParadaMasCongestionada() {
		try {
			return manager.darParadaMasCongestionada();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	
	public static IList<VOParadaCamino> darCaminoMasCorto(String paradaOrigen, String paradaDestino, String hora) {
		return manager.darCaminoMasCorto(paradaOrigen, paradaDestino, hora);
	}

	
	public static IList<VOParadaCamino> darCaminoMenorTiempo(String paradaOrigen, String paradaDestino, String hora) {
		return manager.darCaminoMenorTiempo(paradaOrigen, paradaDestino, hora);
	}

	
	public static IList<VOParada> darMayorComponenteConexa() {
		return manager.darMayorComponenteConexa();
	}

	
	public static IList<VOParadaCamino> darCiclo(String horaInicio) {
		return manager.darCiclo(horaInicio);
	}
	
	public static MST darArbolRecubrimientoMinimo(String horaInicio) {
		manager.simplificarGrafo();
		return manager.darArbolRecubrimientoMinimo(horaInicio);
	}
	 
	
}