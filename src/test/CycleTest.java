package test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.Cycle;
import model.data_structures.DirectedGraph;

public class CycleTest extends TestCase {

	DirectedGraph<Integer, Integer,Integer> grafo;
	protected void setUp() throws Exception {
		grafo = new DirectedGraph<Integer, Integer,Integer>();
		
	}

	public void testDarCiclo() throws Exception {
		grafo.addVertex(1, 1);
		grafo.addVertex(2, 2);
		grafo.addVertex(3, 3);
		grafo.addVertex(4, 4);
		grafo.addVertex(5, 5);
		grafo.addVertex(6, 6);
		grafo.addEdge(1, 3, 1.4);
		grafo.addEdge(1, 2, 1.4);
		grafo.addEdge(2, 5, 1.4);
		grafo.addEdge(3, 4, 1.4);
		grafo.addEdge(3, 5, 1.4);
		grafo.addEdge(4, 5, 1.4);
		grafo.addEdge(5, 1, 1.4);
		
		Cycle<Integer, Integer,Integer> ciclo = new Cycle<Integer, Integer,Integer>(grafo);
		
		assertEquals("WCEX", ciclo.hayCiclo(), true );
		
		Iterator<Integer> ite = ciclo.darCiclo().iterator();
		
		assertEquals("WCEX", ite.next().intValue(), 5 );
		assertEquals("WCEX", ite.next().intValue(), 1 );
		assertEquals("WCEX", ite.next().intValue(), 3 );
		assertEquals("WCEX", ite.next().intValue(), 4 );
		assertEquals("WCEX", ite.next().intValue(), 5 );
	}

}
