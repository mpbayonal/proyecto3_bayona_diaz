package test;

import junit.framework.TestCase;
import model.data_structures.CCStrong;
import model.data_structures.DirectedGraph;

public class DirectedGraphTest extends TestCase {

	DirectedGraph<Integer, Integer,Integer> grafo;
	protected void setUp() throws Exception {
		grafo = new DirectedGraph<Integer, Integer,Integer>();
		
	}
	public void testAddVertex() throws Exception {
		grafo.addVertex(88, 88);
		grafo.addVertex(99, 99);
		grafo.addVertex(66, 66);
		
		
		assertEquals("WCEX", grafo.contieneVertex(88), true );
		assertEquals("WCEX", grafo.contieneVertex(99), true );
		assertEquals("WCEX", grafo.contieneVertex(66), true );
		
	}
	public void testDarVertex() throws Exception {
		grafo.addVertex(88, 88);
		grafo.addVertex(99, 99);
		grafo.addVertex(66, 66);
		
		assertEquals("WCEX", grafo.darVertex(88).intValue(), 88 );
		assertEquals("WCEX", grafo.darVertex(99).intValue(), 99 );
		assertEquals("WCEX", grafo.darVertex(66).intValue(), 66 );
		
	}

	public void testContieneVertex() throws Exception {
		grafo.addVertex(88, 88);
		
		grafo.addVertex(66, 66);
		
		assertEquals("WCEX", grafo.contieneVertex(88), true );
		assertEquals("WCEX", grafo.contieneVertex(99), false );
		assertEquals("WCEX", grafo.contieneVertex(66), true );
		
	}

	

	public void testExisteEdge() throws Exception {
		
		
	
		grafo.addVertex(88, 88);
		
	
		grafo.addVertex(66, 66);
		grafo.addVertex(55, 55);
		grafo.addEdge(88, 66, 1.4);
		grafo.addEdge(88, 55, 3.06);
		
	
		
		
		
		
		assertEquals("WCEX", grafo.existeEdge(88, 66), true );
		assertEquals("WCEX", grafo.existeEdge(88, 55), true );
		assertEquals("WCEX", grafo.existeEdge(88, 33), false );
	}



	public void testAddEdge() throws Exception {
		grafo.addVertex(88, 88);	
		grafo.addVertex(66, 66);
		grafo.addVertex(55, 55);
		grafo.addEdge(88, 66, 1.4);
		grafo.addEdge(88, 55, 3.06);
		
		assertEquals("WCEX", grafo.existeEdge(88, 66), true );
		assertEquals("WCEX", grafo.existeEdge(88, 55), true );
		assertEquals("WCEX", grafo.existeEdge(88, 33), false );
	}

}
