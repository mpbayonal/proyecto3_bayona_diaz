package test;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;

import model.logic.PersistenceManager;

import model.vo.VOBusUpdate;

public class PersistenceManagerTest extends TestCase {

	private PersistenceManager persistenceManager;

	protected void setUp() throws Exception {
		persistenceManager = new PersistenceManager();
	}



	public void testLoadRoutes() throws Exception {

		assertEquals("Se cargaron los elementos del archivo Routes", persistenceManager.loadRoutes("./data/test/routes_test.txt").size(), 246);
	}

	public void testLoadTrips() throws Exception {


		assertEquals("Se cargaron los elementos del archivo Trips", persistenceManager.loadTrips("./data/test/trips_test.txt").size(), 1050);
	}

	

	public void testLoadStops() throws Exception {


		assertEquals("Se cargaron los elementos del archivo Stops", persistenceManager.loadStops("./data/test/stops_test.txt").vertexSize(), 1009);
	}



	public void testLoadEspecialServices() throws Exception {


		assertEquals("Se cargaron los elementos del archivo calendar_dates", persistenceManager.loadEspecialServices("./data/test/calendar_dates.txt").getSize().intValue(), 25);

	}



	public void testReadStopsEstimateService() throws Exception {

		assertEquals("Se cargaron los elementos del archivo stop estimate", 8, persistenceManager.readStopsEstimateService("data/test/stop estimate").getSize().intValue());
	}




	
	public void testLoadShapes() throws Exception {


		assertEquals("Se cargaron los elementos del archivo shapes", persistenceManager.loadShapes("./data/test/shapes.txt").getSize().intValue(), 9);

	}
	public void testLoadInfo() throws Exception {


		assertEquals("Se cargaron los elementos del archivo shapes", persistenceManager.loadInfo("./data/test/feed_info.txt").getSize().intValue(), 1);

	}

}
