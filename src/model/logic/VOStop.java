package model.logic;

import model.data_structures.LPHashTable;

public class VOStop implements Comparable<VOStop>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

	 private int stopId;
	    private Integer stopCode;
	    private String stopName;
	    private String stopDesc;
	    private double stopLat;
	    private double stopLon;
	    private int numeroIncidentes;
	   
	    private LPHashTable<Integer, VOHoraParadaPorParada> tiemposViaje;
	   

	    // -----------------------------------------------------------------
	    // Constructor
	    // -----------------------------------------------------------------

	    public VOStop(int pStopId, Integer pStopCode, String pStopName,  double pStopLat, double pStopLon)
	    {
	        stopId = pStopId;
	        stopCode = pStopCode;
	        stopName = pStopName;
	       
	        stopLat = pStopLat;
	        stopLon = pStopLon;
	        
	        numeroIncidentes = 0;
	     
	        tiemposViaje = new LPHashTable<Integer, VOHoraParadaPorParada>();
	    }

	  
	    
	    @Override
	    public int compareTo(VOStop o)
	    {
	        int r;

	        if(o.getStopId() == stopId) {
	            r=0;
	        }
	        else if(o.getStopId() < stopId) {
	            r=1;
	        }
	        else {
	            r=-1;
	        }

	        return r;
	    }


	    public int hashCode() {
	    	return stopId;
	    }
		public int getStopId() {
			return stopId;
		}



		public void setStopId(int stopId) {
			this.stopId = stopId;
		}



		public Integer getStopCode() {
			return stopCode;
		}



		public void setStopCode(Integer stopCode) {
			this.stopCode = stopCode;
		}



		public String getStopName() {
			return stopName;
		}



		public void setStopName(String stopName) {
			this.stopName = stopName;
		}



		public String getStopDesc() {
			return stopDesc;
		}



		public void setStopDesc(String stopDesc) {
			this.stopDesc = stopDesc;
		}



		public double getStopLat() {
			return stopLat;
		}



		public void setStopLat(double stopLat) {
			this.stopLat = stopLat;
		}



		public double getStopLon() {
			return stopLon;
		}



		public void setStopLon(double stopLon) {
			this.stopLon = stopLon;
		}



		public int getNumeroIncidentes() {
			return numeroIncidentes;
		}



		public void setNumeroIncidentes(int numeroIncidentes) {
			this.numeroIncidentes = numeroIncidentes;
		}



		public LPHashTable<Integer, VOHoraParadaPorParada> getTiemposViaje() {
			return tiemposViaje;
		}



		public void setTiemposViaje(LPHashTable<Integer, VOHoraParadaPorParada> tiemposViaje) {
			this.tiemposViaje = tiemposViaje;
		}

}