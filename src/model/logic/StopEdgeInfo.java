package model.logic;

public class StopEdgeInfo implements Comparable<StopEdgeInfo> 
{
	String hora;
	int viaje;
	double tiempo;
	
	public double getTiempo() {
		return tiempo;
	}
	public void setTiempo(double tiempo) {
		this.tiempo = tiempo;
	}
	public StopEdgeInfo(String hora, int viaje) {
		super();
		this.hora = hora;
		this.viaje = viaje;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public int getViaje() {
		return viaje;
	}
	public void setViaje(int viaje) {
		this.viaje = viaje;
	}
	@Override
	public int compareTo(StopEdgeInfo o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
