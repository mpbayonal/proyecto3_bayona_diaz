package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import api.ISTSManager;
import model.data_structures.DirectedGraph;
import model.data_structures.DoubleLinkedList;
import model.data_structures.EdgeNode;
import model.data_structures.Grafo;
import model.data_structures.IList;
import model.data_structures.LPHashTable;
import model.data_structures.Paths;
import model.data_structures.SeparateChainingHashTable;
import model.vo.Camino;
import model.vo.MST;
import model.vo.VOHoraParadaPorParada;
import model.vo.VOHoraParadaPorViaje;
import model.vo.VOParada;
import model.vo.VOParadaCamino;
import model.vo.VOParadaCongestionada;
import model.vo.VOStop;
import model.vo.VOViaje;





public class STSManager implements ISTSManager {
	
	private PersistenceManager persistencia;
	private LPHashTable<Integer, model.vo.VOTrip> trips;
	public DirectedGraph<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> stops;
	public DirectedGraph<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> subGrafo;

	public void ITSInit() 
	{
		

		persistencia = new PersistenceManager();
		trips = persistencia.loadTrips("./data/trips.txt");
		stops = persistencia.loadStops("./data/stops.txt");
		loadStopTimes("./data/stop_times.txt");
		
		System.out.println("se cargaron los datos");


	}
	
	/*3- Construir un subgrafo de la red de transporte a partir de una parada de origen en una
	fecha y franja horaria dada (a partir de hora inicial HH:MM hasta la hora final HH:MM). El
	subgrafo debe contener las paradas alcanzables desde la parada origen y tomando todas
	las rutas que pasan por la parada tal que el inicio y la llegada a las paradas se encuentren
	en la franja horaria dada. En una parada hay que considerar la posibilidad de continuar en
	la misma ruta o hacer transbordo a rutas diferentes. Los transbordos en una parada a otra
	ruta deben ser posibles en el tiempo; es decir, un transbordo de una ruta a otra debe
	hacerse en un tiempo posterior a la hora de llegada a la parada. Usar la programación
	estática de rutas.
	A partir del subgrafo anterior resolver las siguientes consultas:*/
	
	public void crearSubGrafo(int parada, String horaInicio, String horaFin) throws Exception
	{
		Paths<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> caminos = new Paths<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>>(parada, stops);
		 subGrafo = new DirectedGraph<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>>();
		
		
		Iterator<Integer> paradas = stops.darVertices().iterator();
		while(paradas.hasNext()) 
		{
			int paradaActual = paradas.next();
			if(paradaActual != parada && caminos.existeCamino(paradaActual)) 
			{
				
				Iterator<Integer> ite3 = caminos.camino(paradaActual).iterator();
				
				Integer anterior = ite3.next();
				while(ite3.hasNext()) 
				{
					Integer actual = ite3.next();
					
					if(!subGrafo.existeEdge(anterior, actual)) 
					{
					
					
					EdgeNode<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> temp = stops.darEdge(anterior, actual);
					if(temp!=null) {
					DoubleLinkedList<StopEdgeInfo> lista = darListaRango(temp.getEdgeValue(), horaInicio, horaFin);
					
					if(lista != null) 
					{
						subGrafo.addVertex(stops.darVertex(anterior).getStopId(), stops.darVertex(anterior));
						subGrafo.addVertex(stops.darVertex(actual).getStopId(), stops.darVertex(actual));
						EdgeNode<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> anadir = stops.darEdge(anterior, actual);
						subGrafo.addEdge(anadir.getLlaveActual(), anadir.getLlaveActual(), anadir.getPeso());
						subGrafo.darEdge(anterior, actual).setEdgeValue(lista);
						
					}
					}
					
					}
					
				}
				
				
			}
			
			
		}
		
		
		
	}
	
	
	public DoubleLinkedList<StopEdgeInfo> darListaRango(DoubleLinkedList<StopEdgeInfo> lista, String Inicio, String fin) throws Exception
	{
		Iterator<StopEdgeInfo> ite = lista.iterator();
		DoubleLinkedList<StopEdgeInfo> nueva = new DoubleLinkedList<StopEdgeInfo>();
		while (ite.hasNext()) 
		{
			StopEdgeInfo actual = ite.next();
			
			if(actual.hora.compareTo(Inicio) >= 0 &&  actual.hora.compareTo(fin ) <= 0) 
			{
				nueva.add(actual);
			}
			
			
		}
		
		if(nueva.size() == 0) return null;
		else return nueva;
		
		
		
		
	}

	

	
	public Date parsingDate(String date) throws ParseException 
	{	
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss a");
		DateFormat formatMilitar = new SimpleDateFormat( "HH:mm:ss");
		Date time = null;

		try 
		{
			time = format.parse(date);
		} catch (ParseException e) {

			time = formatMilitar.parse(date);	 
		}

		if (time != null) 
		{
			String formattedDate = formatMilitar.format(time);
		}

		return time;

	}
	
	
	public void loadStopTimes(String stopTimesFile) {


		String cadena;
		VOStopTime anterior = null;
		

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = persistencia.getStringFromCsvLine(datos, 1);
					String departureTime = persistencia.getStringFromCsvLine(datos, 2);
					int stopId = persistencia.getIntFromCsvLine(datos, 3);

					
					int stopSequence = persistencia.getIntFromCsvLine(datos, 4);
					
					Double shapeDistTraveled = 0.0;
					if(datos.length > 8)
					{
						shapeDistTraveled = persistencia.getStringFromCsvLine(datos, 8).isEmpty() ? null : Double.parseDouble(persistencia.getStringFromCsvLine(datos, 8));
					}
					
					VOStopTime newStopTime = new VOStopTime(tripId, arrivalTime, departureTime, stopId, stopSequence, 0, 0, 0, shapeDistTraveled);

					if(newStopTime.getStopSequence()==1) 
					{
						anterior = newStopTime;
					}
					else 
					{
						VOHoraParadaPorParada viaje = new VOHoraParadaPorParada(tripId, arrivalTime, departureTime);
						VOHoraParadaPorViaje parada = new VOHoraParadaPorViaje(arrivalTime, departureTime, stopId);
						viaje.setRouteId(trips.get(tripId).getRouteId());
					
						trips.get(tripId).getTiemposViaje().put(parada.hashCode(), parada);
						if(stops.darVertex(stopId) != null) 
						{
						stops.darVertex(stopId).getTiemposViaje().put(viaje.hashCode(), viaje);
						StopEdgeInfo nuevo = new StopEdgeInfo(arrivalTime, tripId);
						Date hora1 = parsingDate(	anterior.getArrivalTime());
						Date hora2 = parsingDate(	newStopTime.getArrivalTime());
						
						Long hora = hora1.getTime() - hora2.getTime();
						Double horafinall = hora.doubleValue();
						nuevo.setTiempo(horafinall);
						
							if(stops.darEdge(anterior.getStopId(), newStopTime.getStopId()) == null) 
							{
								
								stops.addEdge(anterior.getStopId(), newStopTime.getStopId(), newStopTime.getShapeDistTraveled());
								DoubleLinkedList<StopEdgeInfo> lista = new DoubleLinkedList<StopEdgeInfo>();
								
								lista.add(nuevo);
								stops.darEdge(anterior.getStopId(), newStopTime.getStopId()).setEdgeValue(lista);
								
							}
							else 
							{
								
								stops.darEdge(anterior.getStopId(), newStopTime.getStopId()).getEdgeValue().add(nuevo);
								
							}
						
						
						anterior = newStopTime; }
					}

				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

	}
	
/*	1- Para una parada dada en una fecha específica, retorne una lista de todas las paradas
	alcanzables desde dicha parada. Para cada parada alcanzable, se debe indicar la
	información de la parada desde la cual se llega a ella. Si se debe realizar un transbordo a
	otra ruta para llegar a dicha parada se debe indicar en la respuesta. */

	@Override
	public IList<Camino> darParadasAlcanzables(int parada, String fecha) throws Exception 
	{
		Paths<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> caminos = new Paths<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>>(parada, stops);
		DoubleLinkedList<Camino> lista = new DoubleLinkedList<Camino>();
		
		Iterator<Integer> paradas = stops.darVertices().iterator();
		while(paradas.hasNext()) 
		{
			int paradaActual = paradas.next();
			Camino caminoActual = new Camino();
			if(paradaActual != parada && caminos.existeCamino(paradaActual)) 
			{
				
				Iterator<Integer> ite3 = caminos.camino(paradaActual).iterator();
				
				Integer anterior = ite3.next();
				while(ite3.hasNext()) 
				{
					Integer actual = ite3.next();
					EdgeNode<Integer, model.vo.VOStop, DoubleLinkedList<StopEdgeInfo>> arco = stops.darEdge(anterior, actual);
					
					if(arco.getEdgeValue().getSize() != 0) 
					{
					StopEdgeInfo temp1 = arco.getEdgeValue().darElemento();
					VOParadaCamino arcoActual = new VOParadaCamino(trips.get(temp1.getViaje()), trips.get(temp1.getViaje()), null);
					
					}
					
					
				}
				
				
			}
			
			
		}
		return lista;
	}
	
	/*2- Dar una lista de los componentes fuertemente conexos para toda la red de buses
(utilizando la información estática de las rutas). Por cada componente conexa, dar las
paradas ordenadas por el identificador de la parada.*/

	@Override
	public IList<IList<VOParada>> darComponentesConexas() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public IList<VOStop> darParadasSubGrafo() {
		DoubleLinkedList<VOStop> r= null;
		Iterator<VOStop> iStops= null;
		try {
			iStops = (Iterator<VOStop>) subGrafo.darVertices();
		} catch (Exception e) {
			
			System.out.println("No se puedo cargar el grafo:" + e.getMessage());
		}
		
		while(iStops.hasNext()) {
			VOStop st = iStops.next();
			try {
				r.add( st);
			} catch (Exception e) {
				System.out.println("No se pudo agregar la parada");
			}
			
		}
		
		return r;
	}

	@Override
	public IList<VOHoraParadaPorParada> darItinerarioLlegada(String stopId) {
		model.vo.VOStop parada= null;
		DoubleLinkedList<model.vo.VOHoraParadaPorParada> r= new DoubleLinkedList<>();
		try {
			 parada= subGrafo.darVertex(Integer.parseInt(stopId));
		}
		 catch (Exception e) {
			
			e.printStackTrace();
		}
		Iterator<VOHoraParadaPorParada> iST= parada.getTiemposViaje().iterador();
		while(iST.hasNext()) {
			model.vo.VOHoraParadaPorParada st= iST.next();
			try {
				r.add(st);
			} catch (Exception e) {
				System.out.println("No se pudo agregar la parada");
			}
			
			
		}
			
		return r;
	}

	@Override
	public IList<model.vo.VOHoraParadaPorParada> darItinerarioSalida(String stopId) {
		model.vo.VOStop parada= null;
		DoubleLinkedList<model.vo.VOHoraParadaPorParada> r= new DoubleLinkedList<>();
		try {
			 parada= subGrafo.darVertex(Integer.parseInt(stopId));
		}
		 catch (Exception e) {
			
			e.printStackTrace();
		}
		Iterator<VOHoraParadaPorParada> iST= parada.getTiemposViaje().iterador();
		while(iST.hasNext()) {
			model.vo.VOHoraParadaPorParada st= iST.next();
			try {
				r.add(st);
			} catch (Exception e) {
				System.out.println("No se pudo agregar la parada");
			}
			
			
		}
			
		return r;
	}

	@Override
	
	public VOParadaCongestionada darParadaMasCongestionada() {
		VOParadaCongestionada r= null;
		DoubleLinkedList<VOParadaCongestionada> list= new DoubleLinkedList<>();
		Iterator<VOStop> iStops= darParadasSubGrafo().iterator();
		while(iStops.hasNext()) {
			VOStop st= iStops.next();
			VOParadaCongestionada p= new VOParadaCongestionada(st.getStopId());
			p.setSalidas(darItinerarioSalida(String.valueOf(st.getStopId())));
			p.setLlegadas(darItinerarioLlegada(String.valueOf(st.getStopId())));
			try {
				list.add(p);
			} catch (Exception e) {
				
				System.out.println("error al agregar la parada a la lista");
			}
		}
		
		Iterator<VOParadaCongestionada> rr= list.iterator();
		int mayor = 0;
		while(rr.hasNext()) {
			VOParadaCongestionada pc=rr.next();
			int f= pc.total();
			if(f>mayor) {
				r=pc;
				mayor = f;
			}
			
		}
		return r;
	}

	@Override
	public IList<VOParadaCamino> darCaminoMasCorto(String paradaOrigen, String paradaDestino, String hora) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParadaCamino> darCaminoMenorTiempo(String paradaOrigen, String paradaDestino, String hora) {
		// TODO Auto-generated method stub
		int origen = Integer.parseInt(paradaOrigen);
		int size= subGrafo.vertexSize();
		LPHashTable<Integer, Double> peso = new LPHashTable<>();
		LPHashTable<Integer, Boolean> visitados = new LPHashTable<>();
		LPHashTable<Integer, DoubleLinkedList<VOParadaCamino>> pasos= new LPHashTable<>();
 		
		
		Iterator<VOStop> iStops= null;
		try {
			iStops = (Iterator<VOStop>) subGrafo.darVertices();
		} catch (Exception e) {
			
			System.out.println("No se puedo cargar el grafo:" + e.getMessage());
		}
		while(iStops.hasNext()) {
			VOStop st = iStops.next();
			try {
				peso.put(st.getStopId(),Double.MAX_VALUE);
				visitados.put(st.getStopId(), false);
				DoubleLinkedList<VOParadaCamino> f= new DoubleLinkedList<>();
				pasos.put(st.getStopId(), f);
			} catch (Exception e) {
				System.out.println("No se pudo crear la matriz");
			
			}
			
		}
		peso.put(origen, 0.0);
		visitados.put(origen, true);
		
		recorrer(origen,peso,visitados,pasos);

		return pasos.get(Integer.getInteger(paradaDestino));
	}
	public void recorrer(int origen,LPHashTable<Integer, Double> peso,LPHashTable<Integer, Boolean> visitados,LPHashTable<Integer, DoubleLinkedList<VOParadaCamino>> pasos ) {
		try {
			Iterable<EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>>> adyacentes= subGrafo.darAdyacentes(origen);
			Iterator<EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>>> iA= adyacentes.iterator();
			while(iA.hasNext()) {
				EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>> ady = iA.next();
				int id =ady.getLlaveActual();
				if(!visitados.get(id)) {
					visitados.delete(id);
					visitados.put(id, true);
					if(ady.getPeso()<Double.MAX_VALUE) {
						double sum= peso.get(id)+ ady.getPeso();
						peso.delete(id);
						peso.put(id,sum);
						VOViaje m= null;
						VOParadaCamino way = new VOParadaCamino(m,m);
						pasos.get(id).add(way);
					}
				}
				else {
					recorrer(id,peso,visitados,pasos);
				}
				
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	

	
	/*3.7 Encontrar la componente fuertemente conexa más grande (con mayor cantidad de
			paradas) del subgrafo. La respuesta debe incluir las paradas que la componen.*/

	@Override
	public IList<VOParada> darMayorComponenteConexa() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParadaCamino> darCiclo(String horaInicio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void simplificarGrafo() {
		// TODO Auto-generated method stub
		
	}
	
	/*3.9 A partir de una hora inicial (HH:MM en la franja horaria), simplificar el subgrafo para
	que para aquellas parejas de paradas que estén conectadas por arcos de ida y de regreso
	en algún momento a partir de una hora inicial, se puedan reducir a un único arco no
	dirigido (este arco modela una ida/regreso entre paradas con la restricción de tiempo
	dada). El arco resultante tendrá como peso el tiempo promedio de todos los viajes que
	conectan ambas paradas. En el subgrafo No dirigido resultante, encontrar el Arbol de
	Recubrimiento Mínimo (MST). La respuesta debe incluir la secuencia de arcos que definen
	el MST resultante y el valor de tiempo para el MST. Por cada arco hay que incluir los Ids de
	las paradas conectadas y el tiempo de conexión entre ambas paradas. */

	@Override
	public MST darArbolRecubrimientoMinimo(String horaInicio) throws Exception 
	{
		Iterator<Integer> ite1 = subGrafo.darVertices().iterator();
		Grafo<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>> h = new Grafo<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>>();
		while(ite1.hasNext()) 
		{
			Integer vertice = ite1.next();
			Iterator<EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>>> ite2 = subGrafo.darAdyacentes(vertice).iterator();
			while(ite2.hasNext()) 
			{
				EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>> verticeDestino = ite2.next();
				
				if(subGrafo.existeEdge(verticeDestino.getDest(), vertice));
				{
					EdgeNode<Integer, VOStop, DoubleLinkedList<StopEdgeInfo>> verticeDestino2 = subGrafo.darEdge(verticeDestino.getDest(), vertice);
					DoubleLinkedList<StopEdgeInfo> lista = mayorRango(verticeDestino.getEdgeValue(), verticeDestino2.getEdgeValue(), horaInicio);
					if(lista != null) 
					{
						
						h.addVertex(vertice, subGrafo.darVertex(vertice));
						h.addVertex(verticeDestino.getDest(), subGrafo.darVertex(verticeDestino.getDest()));
						
						h.addEdge(vertice, verticeDestino.getDest(), darPromediohora(lista));
						
						
					}
							
					
					
				}
				
			}
			
		}
		
		return h.darMst();

	}
	
	public double darPromediohora(DoubleLinkedList<StopEdgeInfo> lista) 
	{
		Iterator<StopEdgeInfo> ite = lista.iterator();
		Double suma = 0.0;
		int total = 0;
		
		while (ite.hasNext()) 
		{
			StopEdgeInfo actual = ite.next();
			
			suma += actual.getTiempo();
			total++;
			
		}
		
		return suma/total;
		
		
	}
	
	public DoubleLinkedList<StopEdgeInfo> mayorRango(DoubleLinkedList<StopEdgeInfo> lista,DoubleLinkedList<StopEdgeInfo> lista2, String Inicio) throws Exception
	{
		Iterator<StopEdgeInfo> ite = lista.iterator();
		Iterator<StopEdgeInfo> ite2 = lista2.iterator();
		DoubleLinkedList<StopEdgeInfo> nueva = new DoubleLinkedList<StopEdgeInfo>();
		while (ite.hasNext()) 
		{
			StopEdgeInfo actual = ite.next();
			
			if(actual.hora.compareTo(Inicio) >= 0 ) 
			{
				nueva.add(actual);
			}
			
			
		}
		
		while (ite2.hasNext()) 
		{
			StopEdgeInfo actual2 = ite2.next();
			
			if(actual2.hora.compareTo(Inicio) >= 0 ) 
			{
				nueva.add(actual2);
			}
			
			
		}
		
		if(nueva.size() == 0) return null;
		else return nueva;
		
		
		
		
	}


	@Override
	public void ITScargarGTFS() {
		// TODO Auto-generated method stub
		
	}

}
