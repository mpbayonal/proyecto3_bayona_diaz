package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class VORangoHora implements Comparable<VORangoHora> {
	
	private int horaInicial;
	private int horaFinal;
	
	/**
	 * Lista de retardos ordenados por tiempo total de retardo
	 */
	DoubleLinkedList<VORetardoViaje> listaRetardos;

	public VORangoHora(int hora) {
		horaInicial=hora;
		horaFinal=++hora;
		listaRetardos= new DoubleLinkedList<VORetardoViaje>() ;
		
	}

	public int getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(int horaInicial) {
		this.horaInicial = horaInicial;
	}

	public int getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	public IList<VORetardoViaje> getListaRetardos() {
		return listaRetardos;
	}

	public void setListaRetardos(DoubleLinkedList<VORetardoViaje> listaRetardos) {
		this.listaRetardos = listaRetardos;
	}
	
	public int compareTo(VORangoHora o) {
		return 0;
	}

	public void addRetardoViaje(VORetardoViaje retardoViaje) {
		try {
			listaRetardos.add(retardoViaje);
		} catch (Exception e) {
			
		}
		
	}

	public int totalRetardo() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}
