package model.vo;

import java.util.Date;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class VOTransbordo implements Comparable<VOTransbordo>
{
	/**
	 * Modela el tiempo de transbordo
	 */
	private Date transferTime;
	
	/**
	 * Id de la parada de origen.
	 */
	private Integer idParadaOrigen;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private DoubleLinkedList<VOViajesRuta4C> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public Date getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(Date transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public DoubleLinkedList<VOViajesRuta4C> getListadeParadas()
	{
		return listadeParadas;
	}

	public Integer getIdParadaOrigen() {
		return idParadaOrigen;
	}

	public void setIdParadaOrigen(Integer idParadaOrigen) {
		this.idParadaOrigen = idParadaOrigen;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(DoubleLinkedList<VOViajesRuta4C> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}

	public VOTransbordo(Date transferTime, Integer idParadaOrigen) {
		super();
		this.transferTime = transferTime;
		this.idParadaOrigen = idParadaOrigen;
		listadeParadas = new DoubleLinkedList<VOViajesRuta4C>();
	}

	@Override
	public int compareTo(VOTransbordo o) {
		
		return transferTime.compareTo(o.getTransferTime());
	}
	

}
