package model.vo;

public class VOServicio implements Comparable<VOServicio>
{
	public VOServicio(int serviceId, Double distanciaRecorrida) {
		super();
		this.serviceId = serviceId;
		this.distanciaRecorrida = distanciaRecorrida;
	}



	/**
	 * Modela el id del servicio
	 */
     private int serviceId;

     /**
      * Modela la sistancia recorrida del servicio dado una fecha (req 2C)
      */
     private Double distanciaRecorrida;
     
	/**
	 * @return the serviceId
	 */
	public int getServiceId() 
	{
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(int serviceId) 
	{
		this.serviceId = serviceId;
	}

	/**
	 * @return the distanciaRecorrida
	 */
	public Double getDistanciaRecorrida()
	{
		return distanciaRecorrida;
	}

	/**
	 * @param distanciaRecorrida the distanciaRecorrida to set
	 */
	public void setDistanciaRecorrida(Double distanciaRecorrida)
	{
		this.distanciaRecorrida = distanciaRecorrida;
	}

	

	@Override
	public int compareTo(VOServicio o) {
		 int r;

	        if(o.getServiceId() == serviceId) {
	            r=0;
	        }
	        else if(o.getServiceId() < serviceId) {
	            r=1;
	        }
	        else {
	            r=-1;
	        }

	        return r;
	}
}
