package model.vo;

import com.google.gson.annotations.SerializedName;

public class VORouteEstimate implements Comparable<VORouteEstimate>
{
	public VORouteEstimate(String routeNo, String routeName, String direction, UrlRoute routeMap,
			VOSchedules[] schedules) {
		super();
		RouteNo = routeNo;
		RouteName = routeName;
		Direction = direction;
		RouteMap = routeMap;
		Schedules = schedules;
	}

	@SerializedName("RouteNo") private String RouteNo;
	@SerializedName("RouteName") private String RouteName;
	@SerializedName("Direction") private String Direction;
	@SerializedName("RouteMap") private UrlRoute RouteMap;
	@SerializedName("Schedules") private VOSchedules[] Schedules;
	
	@Override
	public int compareTo(VORouteEstimate o) {
		// TODO Auto-generated method stub
		return RouteNo.compareTo(o.getRouteNo());
	}

	public String getRouteNo() {
		return RouteNo;
	}

	public void setRouteNo(String routeNo) {
		RouteNo = routeNo;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

	public UrlRoute getRouteMap() {
		return RouteMap;
	}

	public void setRouteMap(UrlRoute routeMap) {
		RouteMap = routeMap;
	}

	public VOSchedules[] getSchedules() {
		return Schedules;
	}

	public void setSchedules(VOSchedules[] schedules) {
		Schedules = schedules;
	}

}
