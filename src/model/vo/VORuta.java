package model.vo;

public class VORuta implements Comparable<VORuta>
{
	//Atributos
	
	/**
	 * Modela el id de la ruta
	 */
	 private String idRoute;

	 /**
	  * Model el short name de la ruta
	  */
	 private String shortName;
	 
	 //M�todos
	 
	/**
	 * @return the idRoute
	 */
	public String getIdRoute() 
	{
		return idRoute;
	}

	/**
	 * @param idRoute the idRoute to set
	 */
	public void setIdRoute(String idRoute) 
	{
		this.idRoute = idRoute;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() 
	{
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	@Override
	public int compareTo(VORuta o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
