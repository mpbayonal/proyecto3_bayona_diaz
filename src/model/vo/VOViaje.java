package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOViaje implements Comparable<VOViaje>
{
	DoubleLinkedList<VOParadaTiempo> paradas;
	public VOViaje( int idViaje) {
		super();
		
		this.idViaje = idViaje;
		paradas = new DoubleLinkedList<VOParadaTiempo>();
	}

	public DoubleLinkedList<VOParadaTiempo> getParadas() {
		return paradas;
	}
	
	  public int hashCode()
	    {
	    		return idViaje;
	    }

	public void setParadas(DoubleLinkedList<VOParadaTiempo> paradas) {
		this.paradas = paradas;
	}

	/**
	 * Modela el id del viaje
	 */
	private int idViaje;
	private int tiempoRetraso;

	public int getTiempoRetraso() {
		return tiempoRetraso;
	}

	public void setTiempoRetraso(int tiempoRetraso) {
		this.tiempoRetraso = tiempoRetraso;
	}

	/**
	 * @return the idViaje
	 */
	public int getIdViaje()
	{
		return idViaje;
	}

	/**
	 * @param idViaje the idViaje to set
	 */
	public void setIdViaje(int idViaje)
	{
		this.idViaje = idViaje;
	}

	@Override
	public int compareTo(VOViaje o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
