package model.vo;

public class VORetardoViaje implements Comparable<VORetardoViaje>{
	
	private int viajeid;
	private int tiempoRetardo;
	
	public VORetardoViaje(int id) {
		viajeid=id;
		tiempoRetardo=0;
	}
	
	public int getViajeid() {
		return viajeid;
	}
	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}
	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}
	public void adicionarTiempo(int t) {
		tiempoRetardo+=t;
	}

	@Override
	public int compareTo(VORetardoViaje o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
