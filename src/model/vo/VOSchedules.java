package model.vo;

import com.google.gson.annotations.SerializedName;

public class VOSchedules {
	
	public VOSchedules(String pattern, String destination, String expectedLeaveTime, int expectedCountdown,
			String scheduleStatus, boolean cancelledTrip, boolean addedTrip, boolean addedStop, boolean cancelledStop,
			String lastUpdate) {
		super();
		Pattern = pattern;
		Destination = destination;
		ExpectedLeaveTime = expectedLeaveTime;
		ExpectedCountdown = expectedCountdown;
		ScheduleStatus = scheduleStatus;
		CancelledTrip = cancelledTrip;
		AddedTrip = addedTrip;
		AddedStop = addedStop;
		CancelledStop = cancelledStop;
		LastUpdate = lastUpdate;
	}
	@SerializedName("Pattern") private String Pattern;
	@SerializedName("Destination") private String Destination;
	@SerializedName("ExpectedLeaveTime")private String ExpectedLeaveTime;
	@SerializedName("ExpectedCountdown")private int ExpectedCountdown;
	@SerializedName("ScheduleStatus")private String ScheduleStatus;
	@SerializedName("CancelledTrip")private boolean CancelledTrip;
	@SerializedName("AddedTrip")private boolean AddedTrip;
	@SerializedName("AddedStop")private boolean AddedStop;
	@SerializedName("CancelledStop")private boolean CancelledStop;
	@SerializedName("LastUpdate")private String LastUpdate;
	public String getPattern() {
		return Pattern;
	}
	public void setPattern(String pattern) {
		Pattern = pattern;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	public String getExpectedLeaveTime() {
		return ExpectedLeaveTime;
	}
	public void setExpectedLeaveTime(String expectedLeaveTime) {
		ExpectedLeaveTime = expectedLeaveTime;
	}
	public int getExpectedCountdown() {
		return ExpectedCountdown;
	}
	public void setExpectedCountdown(int expectedCountdown) {
		ExpectedCountdown = expectedCountdown;
	}
	public String getScheduleStatus() {
		return ScheduleStatus;
	}
	public void setScheduleStatus(String scheduleStatus) {
		ScheduleStatus = scheduleStatus;
	}
	public boolean isCancelledTrip() {
		return CancelledTrip;
	}
	public void setCancelledTrip(boolean cancelledTrip) {
		CancelledTrip = cancelledTrip;
	}
	public boolean isAddedTrip() {
		return AddedTrip;
	}
	public void setAddedTrip(boolean addedTrip) {
		AddedTrip = addedTrip;
	}
	public boolean isAddedStop() {
		return AddedStop;
	}
	public void setAddedStop(boolean addedStop) {
		AddedStop = addedStop;
	}
	public boolean isCancelledStop() {
		return CancelledStop;
	}
	public void setCancelledStop(boolean cancelledStop) {
		CancelledStop = cancelledStop;
	}
	public String getLastUpdate() {
		return LastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}
	

}
