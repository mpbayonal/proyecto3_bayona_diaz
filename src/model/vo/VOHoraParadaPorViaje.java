package model.vo;

public class VOHoraParadaPorViaje implements Comparable<VOHoraParadaPorViaje>
{
	
	    private String arrivalTime;
	    private String departureTime;
	    private int stopId;
	
		public String getArrivalTime() {
			return arrivalTime;
		}
		
		public int hashCode() {
	    	return stopId;
	    }

		public void setArrivalTime(String arrivalTime) {
			this.arrivalTime = arrivalTime;
		}

		public String getDepartureTime() {
			return departureTime;
		}

		public void setDepartureTime(String departureTime) {
			this.departureTime = departureTime;
		}

		public int getStopId() {
			return stopId;
		}

		public void setStopId(int stopId) {
			this.stopId = stopId;
		}

		public VOHoraParadaPorViaje(String arrivalTime, String departureTime, int stopId) {
			super();
			this.arrivalTime = arrivalTime;
			this.departureTime = departureTime;
			this.stopId = stopId;
		}

		public int compareTo(VOHoraParadaPorViaje o) {
			// TODO Auto-generated method stub
			return 0;
		}

}
