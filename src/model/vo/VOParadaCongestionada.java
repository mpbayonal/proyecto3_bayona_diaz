package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class VOParadaCongestionada implements Comparable<VOParadaCongestionada> {

	
	public DoubleLinkedList<VOHoraParadaPorParada> llegadas;
	public DoubleLinkedList<VOHoraParadaPorParada> salidas;
	public int id;
	
	
	
	public VOParadaCongestionada(int pid) {
		id= pid;
		salidas= new DoubleLinkedList<VOHoraParadaPorParada>();
		llegadas= new DoubleLinkedList<VOHoraParadaPorParada>();
	}
	
	public void agregarLlegada(VOHoraParadaPorParada p) {
		try {
			llegadas.add(p);
		} catch (Exception e) {
		
			System.out.println("no se pudo agregar la llegasa");
		}
	}
	public void agregarSalida(VOHoraParadaPorParada p) {
		try {
			salidas.add(p);
		} catch (Exception e) {
		
			System.out.println("no se pudo agregar la llegasa");
		}
	}
	public void setLlegadas(IList<VOHoraParadaPorParada> iList) {
		llegadas= (DoubleLinkedList<VOHoraParadaPorParada>) iList;
	}
	public void setSalidas(IList<VOHoraParadaPorParada> iList) {
		salidas= (DoubleLinkedList<VOHoraParadaPorParada>) iList;
	}
	public int total() {
		return salidas.size()+ llegadas.size();
	}
	
	
	/**
	 * Dar una representaci�n de la parada 
	 */
	public String toString() {
		//TODO
		return "";
	}

	@Override
	public int compareTo(VOParadaCongestionada o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
