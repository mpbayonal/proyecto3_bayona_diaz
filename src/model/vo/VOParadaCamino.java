

package model.vo;

import java.sql.Time;

public class VOParadaCamino implements Comparable<VOParadaCamino> {

	public VOParadaCamino(VOTrip viajeLlegada, VOTrip viajeSalida , Time hora) {
		super();
		this.viajeLlegada = viajeLlegada;
		this.viajeSalida = viajeSalida;
		horaLlegada = hora;
		horaSalida = hora;
	}
	public VOTrip getViajeLlegada() {
		return viajeLlegada;
	}
	public void setViajeLlegada(VOTrip viajeLlegada) {
		this.viajeLlegada = viajeLlegada;
	}
	public Time getHoraLlegada() {
		return horaLlegada;
	}
	public void setHoraLlegada(Time horaLlegada) {
		this.horaLlegada = horaLlegada;
	}
	public VOTrip getViajeSalida() {
		return viajeSalida;
	}
	public void setViajeSalida(VOTrip viajeSalida) {
		this.viajeSalida = viajeSalida;
	}
	public Time getHoraSalida() {
		return horaSalida;
	}
	public void setHoraSalida(Time horaSalida) {
		this.horaSalida = horaSalida;
	}
	private VOTrip viajeLlegada;
	private Time horaLlegada;
	
	private VOTrip viajeSalida;
	private Time horaSalida;
	@Override
	public int compareTo(VOParadaCamino o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
