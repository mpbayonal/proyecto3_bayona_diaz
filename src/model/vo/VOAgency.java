package model.vo;

public class VOAgency implements Comparable<VOAgency> 
{
	public VOAgency(String id, String name, String url, String timezone, String lang) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.timezone = timezone;
		this.lang = lang;
	}

	private String id;
	private String name;
	private String url;
	private String timezone;
	private String lang;

	
	public int compareTo(VOAgency o) {
		return id.compareTo(o.getId()) ;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
