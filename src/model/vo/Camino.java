package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class Camino implements Comparable<Camino> {

	public DoubleLinkedList<VOParadaCamino> paradasEnCamino;
	
	
	public IList<VOParada> paradaAnterior;

	public Camino() {
		paradasEnCamino = new DoubleLinkedList<VOParadaCamino>();
	}

	public DoubleLinkedList<VOParadaCamino> getParadasEnCamino() {
		return paradasEnCamino;
	}

	public void setParadasEnCamino(DoubleLinkedList<VOParadaCamino> paradasEnCamino) {
		this.paradasEnCamino = paradasEnCamino;
	}

	@Override
	public int compareTo(Camino o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
