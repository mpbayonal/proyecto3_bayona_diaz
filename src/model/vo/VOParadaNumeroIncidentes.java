package model.vo;

public class VOParadaNumeroIncidentes implements Comparable<VOParadaNumeroIncidentes>
{
	//Atributos
	/**
	 * Modela el id de la parada
	 */
	private int stopId;
	
	/**
	 * Modela el n�mero de incidentes
	 */
	private int numeroIncidentes;
	private int tiempoRetardo;
	
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}

	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}

	//M�todos
	/**
	 * @return the stopId
	 */
	public int getStopId()
	{
		return stopId;
	}

	/**
	 * @param stopId the stopId to set
	 */
	public void setStopId(int stopId) 
	{
		this.stopId = stopId;
	}

	public VOParadaNumeroIncidentes(int stopId) {
		super();
		this.stopId = stopId;
		this.numeroIncidentes = 0;
		this.tiempoRetardo = 0;
	}

	/**
	 * @return the numeroIncidentes
	 */
	public int getNumeroIncidentes()
	{
		return numeroIncidentes;
	}
	
	public void agregarIncidente() {
		numeroIncidentes++;
	}
	

	/**
	 * @param numeroIncidentes the numeroIncidentes to set
	 */
	public void setNumeroIncidentes(int numeroIncidentes)
	{
		this.numeroIncidentes = numeroIncidentes;
	}

	@Override
	public int compareTo(VOParadaNumeroIncidentes o) {
		
		int r;
		 if(o.getNumeroIncidentes() == numeroIncidentes) {
	            r=0;
	        }
	        else if(o.getNumeroIncidentes() < numeroIncidentes) {
	            r=1;
	        }
	        else {
	            r=-1;
	        }

	        return r;
	}
}
