package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;


	public class VOParada  implements Comparable<VOParada>
	{
		public VOParada() {
			viajesDeRutas = new DoubleLinkedList<VOViajesRuta4C>();
			viajes = new DoubleLinkedList<VOViaje>();
		}

		public DoubleLinkedList<VOViaje> getViajes() {
			return viajes;
		}

		public void setViajes(DoubleLinkedList<VOViaje> viajes) {
			this.viajes = viajes;
		}

		//Atributos
		/**
		 * Modela el id de la parada
		 */
		private int stopId;
		
		/**
		 * Modela el n�mero de incidentes
		 */
		private int numeroIncidentes;
		
		/**
		 * Si la parada es compartida, esta lista tiene las rutas que la comparten
		 * , cada una con los viajes que usan la parada. 
		 */
		private IList<VOViajesRuta4C> viajesDeRutas;
		
		public IList<VOViajesRuta4C> getViajesDeRutas() {
			return viajesDeRutas;
		}
		
		public DoubleLinkedList<VOViaje> viajes;

		public void setViajesDeRutas(IList<VOViajesRuta4C> viajesDeRutas) {
			this.viajesDeRutas = viajesDeRutas;
		}

		//M�todos
		/**
		 * @return the stopId
		 */
		public int getStopId()
		{
			return stopId;
		}

		/**
		 * @param stopId the stopId to set
		 */
		public void setStopId(int stopId) 
		{
			this.stopId = stopId;
		}

		/**
		 * @return the numeroIncidentes
		 */
		public int getNumeroIncidentes()
		{
			return numeroIncidentes;
		}

		/**
		 * @param numeroIncidentes the numeroIncidentes to set
		 */
		public void setNumeroIncidentes(int numeroIncidentes)
		{
			this.numeroIncidentes = numeroIncidentes;
		}
		
		public void aumentarIncidentes() 
		{
			numeroIncidentes++;
		}

		@Override
		public int compareTo(VOParada o) {
			// TODO Auto-generated method stub
			return 0;
		}
	}


