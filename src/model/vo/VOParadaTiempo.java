package model.vo;

public class VOParadaTiempo implements Comparable<VOParadaTiempo>
{
	//Atributos
		/**
		 * Modela el id de la parada
		 */
		private int stopId;
		
		/**
		 * Modela el n�mero de incidentes
		 */
		private int numeroIncidentes;
		private int tiempoRetardo;
		
		public int getTiempoRetardo() {
			return tiempoRetardo;
		}

		public void setTiempoRetardo(int tiempoRetardo) {
			this.tiempoRetardo = tiempoRetardo;
		}

		//M�todos
		/**
		 * @return the stopId
		 */
		public int getStopId()
		{
			return stopId;
		}

		/**
		 * @param stopId the stopId to set
		 */
		public void setStopId(int stopId) 
		{
			this.stopId = stopId;
		}

		public VOParadaTiempo(int stopId, int tiempoRetardo) {
			super();
			this.stopId = stopId;
			this.tiempoRetardo = tiempoRetardo;
			numeroIncidentes = 0;
		}

		/**
		 * @return the numeroIncidentes
		 */
		public int getNumeroIncidentes()
		{
			return numeroIncidentes;
		}

		/**
		 * @param numeroIncidentes the numeroIncidentes to set
		 */
		public void setNumeroIncidentes(int numeroIncidentes)
		{
			this.numeroIncidentes = numeroIncidentes;
		}

	

		@Override
		public int compareTo(VOParadaTiempo o) {
			int r;
			 if(o.getTiempoRetardo() == numeroIncidentes) {
		            r=0;
		        }
		        else if(o.getNumeroIncidentes() < numeroIncidentes) {
		            r=1;
		        }
		        else {
		            r=-1;
		        }

		        return r;
		}
}
