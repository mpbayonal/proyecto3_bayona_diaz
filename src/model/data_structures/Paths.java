package model.data_structures;

import java.util.Iterator;

public class Paths<K extends Comparable<K>, V, E >  
{
	private SeparateChainingHashTable<K, Boolean>  marcados;
	private SeparateChainingHashTable<K, K>  enLaPila;
	private K source;
	
	public Paths(K source, DirectedGraph<K, V, E> grafo) throws Exception {
		
		this.source = source;
		
		marcados = new SeparateChainingHashTable<K, Boolean>();
		enLaPila = new SeparateChainingHashTable<K, K>();
		
		
		Iterator<K> ite = grafo.darVertices().iterator();
		
		while (ite.hasNext()) 
		{
			K temp = ite.next();
			marcados.put(temp, false);
			enLaPila.put(temp, null);
			
			
		}
		
		dfs(grafo, source);
		
		
	}
	
	

	
	private void dfs(DirectedGraph<K,V,E> Grafo, K v) throws Exception
	{
	
	marcados.put(v, true);
	Iterator<EdgeNode<K, V,E>>  ite = Grafo.darAdyacentes(v).iterator();
		while(ite.hasNext()) 
		{
			EdgeNode<K, V,E> nodo = ite.next();
			if (!existeCamino(nodo.dest)) 
			{
				enLaPila.put(nodo.getDest(), v);
				dfs(Grafo, nodo.dest);
			}		
		}
	
	}

	public Iterable<K> camino(K v) throws Exception
	{
	if (!existeCamino(v)) return null;
	StackIterable<K> camino = new StackIterable<K>();
	
	for (K x = v; x != source; x = enLaPila.get(x)) 
	{
		camino.push(x);
	}
	camino.push(source);

	return camino;
	}
	
	
	
	
	public Boolean existeCamino(K llave) throws Exception 
	{
		return marcados.get(llave);
	}
	
}

