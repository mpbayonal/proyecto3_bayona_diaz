package model.data_structures;

import java.util.Arrays;

public class PriorityQueueMax <K  extends Comparable<K> , V >
{
	public PriorityQueueMax() 
	{
		tamano = 0;
		tamanoArreglo = 500;
		arreglo = (HashNode<K, V> []) new HashNode[tamanoArreglo];
	
		

	}

	private int tamano;// numero de elementos en la cola
	private HashNode<K,V>[] arreglo;
	private int tamanoArreglo;// tamaño con que se creo el arreglo

	public V darElementoMayor() 
	{
		V elemento = null;

		if (tamano != 0) 
		{
			elemento = arreglo[1].getValue();
		}

		return elemento;

	}
	


	public V dequeueElementoMayor() 
	{
		V elemento = null;

		if (tamano != 0) 
		{
			elemento = arreglo[1].getValue();
			intercambiar(1, tamano);
			tamano--;
			sink(1);		

		}



		return elemento;

	}
	
	public void enqueue(K llave ,V elemento) 
	{
		if(tamano >= tamanoArreglo-1) 
		{
			aumentarTamanoArreglo();
		}
		tamano++;
	HashNode<K, V> newE = new HashNode<>(llave, elemento);
		arreglo[tamano] = newE;
		swim(tamano);
		
	}

	
	
	
	private void sink(int i)
	{
		int indice = i;
		while(indice * 2  <= tamano) 
		{
			int hijo = 2*indice;
			if( hijo < tamano &&  esMenor(hijo, hijo+1)) 
			{
				hijo++;
			}
			if( !esMenor(indice, hijo)) 
			{
				break;
			}
			
			intercambiar(indice, hijo);
			indice = hijo;
			
		}
	}
	
	private void swim(int i)
	{
		int indice = i;
		HashNode<K, V> elemento= arreglo[tamano];
		while(indice > 1 && esMenor(indice/2, indice))
		{
			intercambiar(indice, indice/2);
			indice =  indice/2;
		}
	}

	
	private void aumentarTamanoArreglo()
	{		
		arreglo = Arrays.copyOf(arreglo, arreglo.length*2);
		tamanoArreglo = arreglo.length;		
	}


	private boolean esRaiz(int indice) 
	{
		
		return !(indice > 1);
	}

	private boolean tieneHijoIzquierdo(int indice)
	{
		return indice*2 < tamano;
		
	}



	private void intercambiar(int index1 , int index2)
	{
		HashNode<K, V> elemento = arreglo[index2];
		arreglo[index2] = arreglo[index1];
		arreglo[index1] = elemento;


	}

	private boolean esMenor(int indice1, int indice2)
	{
		return arreglo[indice1].darLlave().compareTo(arreglo[indice2].darLlave()) < 0;  

	}

	public boolean isEmpty()
	{
		return tamano == 0;
	}
	
	

	public int getSize()
	{
		return tamano;
	}
	
	
}
