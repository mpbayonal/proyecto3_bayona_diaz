package model.data_structures;

public class T23NodeS<K extends Comparable<K>, V> {
	private K key;
	private V value;
	public T23NodeS(K pKey, V pValue) {
		this.key=pKey;
		this.value=pValue;
	}
	public K getKey() {
		return key;
	}
	public V getValue() {
		return value;
	}
	public void changeV(V p) {
		this.value=p;
	}
	public void changeK(K p) {
		this.key=p;
	}
}
