package model.data_structures;

import java.util.Iterator;

public class ShortestPath<K extends Comparable<K>, V, E >  
{
	 private static final int INFINITY = Integer.MAX_VALUE;
	     // distTo[v] = number of edges shortest s-v path
	    
	    private SeparateChainingHashTable<K, Boolean>  marked;
		private SeparateChainingHashTable<K, K>  edgeTo;
		private SeparateChainingHashTable<K, Integer>  distTo;
		

	    /**
	     * Computes the shortest path between the source vertex {@code s}
	     * and every other vertex in the graph {@code G}.
	     * @param G the graph
	     * @param s the source vertex
	     * @throws Exception 
	     * @throws IllegalArgumentException unless {@code 0 <= s < V}
	     */
	    public ShortestPath(K source, DirectedGraph<K, V, E> grafo) throws Exception {
	   
			
	    	marked = new SeparateChainingHashTable<K, Boolean>();
	    	edgeTo = new SeparateChainingHashTable<K, K>();
	    	distTo = new SeparateChainingHashTable<K, Integer>();
			
			
			Iterator<K> ite = grafo.darVertices().iterator();
			
			
			while (ite.hasNext()) 
			{
				K temp = ite.next();
				marked.put(temp, false);
				edgeTo.put(temp, null);
				distTo.put(temp, -5);
				
			
		        
		        
		        
				
				
			}
	        
	        bfs(grafo, source );

	       
	    }

	


	    // breadth-first search from a single source
	    private void bfs( DirectedGraph<K, V, E> grafo, K source) throws Exception {
	    		
	    		
	    	Iterator<K> ite = grafo.darVertices().iterator();
			QueueIterable<K> q = new QueueIterable<K>();
			
			while (ite.hasNext()) 
			{
				K temp = ite.next();
				
				distTo.put(temp, INFINITY);
				
				
				marked.put(source, true);
				
				distTo.put(source, 0);
				
		        q.enqueue(source);
		        
		        
		        while (!q.isEmpty()) {
		            K v = q.dequeue();
		            
		            Iterator<EdgeNode<K, V,E>>  ite2 = grafo.darAdyacentes(v).iterator();
		    		while(ite2.hasNext()) 
		    		{
		    			EdgeNode<K, V,E> nodo = ite2.next();
		    			
		    			if (nodo != null&&!marked.get(nodo.dest)) 
		    			{
		    				edgeTo.put(nodo.dest, v);
		    				Integer temp3 = distTo.get(v)+1;
		    				distTo.put(nodo.dest, temp3);
		    				marked.put(nodo.dest, true) ;
		    				q.enqueue(nodo.dest);
		    				
		    				
		    			}		
		    		}
		            
		          
		        } 
				
				
			}
	    		
	     

	       
	    }

	  
	  
	    public boolean hasPathTo(K v) throws Exception {
	        
	        return marked.get(v);
	    }

	    public int distTo(K v) throws Exception {
	        
	        return distTo.get(v);
	    }


	    public Iterable<K> pathTo(K v) throws Exception {
	       
	        if (!hasPathTo(v)) return null;
	        StackIterable<K> path = new StackIterable<K>();
	        K x;
	        for (x = v; distTo.get(x) != 0; x = edgeTo.get(x))
	            path.push(x);
	        path.push(x);
	        return path;
	    }


	  
	
	
	
	
	

}
