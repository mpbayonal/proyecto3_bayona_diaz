package model.data_structures;

import java.util.Iterator;

import model.vo.MST;

public class Grafo<K extends Comparable<K>, V, E >
{
	
	private SeparateChainingHashTable<K, EdgeNode<K, V, E> > neighbors;
	private int vertexSize;
	private int edgesSize;




	public Grafo() {

		vertexSize = 0;
		edgesSize = 0;
		neighbors = new SeparateChainingHashTable<K, EdgeNode<K, V, E> >();
	}
	
	public Iterable<EdgeNode<K, V,E>> darAdyacentes(K llave) throws Exception
	{
		EdgeNode<K, V,E> temp = neighbors.get(llave);
		QueueIterable<EdgeNode<K, V,E>> cola = new QueueIterable<EdgeNode<K, V,E>>();
		
		while(temp != null && temp.dest != null  ) 
		{
			cola.enqueue(temp);
			temp = temp.getSiguiente();
		}
		
		return cola;
		
	}
	
	public MST darMst() 
	{
		MST h = new MST();
		return h;
	}
	public Iterable<K> darVertices() throws Exception
	{
		Iterator<EdgeNode<K, V,E>> temp = neighbors.iterador();
		QueueIterable<K> cola = new QueueIterable<K>();
		
		while(temp.hasNext()  ) 
		{ 	
			EdgeNode<K, V,E> nodo = temp.next();
			
			cola.enqueue(nodo.llaveActual);
			
		}
		
		return cola;
		
	}
	
	public V darVertex(K llave) throws Exception 
	{
		if(neighbors.get(llave)!= null)
		return neighbors.get(llave).getValor();
		else return null;
	}
	
	public boolean contieneVertex(K llave) throws Exception 
	{
		return darVertex(llave) != null;
	}



	public void addVertex(K id, V info) throws Exception 
	{
		EdgeNode<K, V,E> actual = neighbors.get(id);
		if(actual == null) 
		{
			EdgeNode<K, V,E> nuevo = new EdgeNode<K, V,E>(id, null, info, 0.0);
			neighbors.put(id, nuevo);
			vertexSize ++;
		}
		else 
		{
			actual.setValor(info);
		}
		
		
	}


	public boolean existeEdge(K source, K dest) throws Exception
	{
		boolean existe = false;
		if(neighbors.contiene(source)) 
		{ 	  
			EdgeNode<K, V,E> temp = neighbors.get(source);
			while(temp != null)
			{
				if(temp.getDest() != null && temp.getDest().compareTo(dest) == 0) 
				{
					existe = true;
				}

				temp = temp.getSiguiente();
			}
		}

		return existe;   		

	}
	
	public EdgeNode<K, V,E> darEdge(K source, K dest) throws Exception
	{
		EdgeNode<K, V,E> existe = null;
		if(neighbors.contiene(source)) 
		{ 	  
			EdgeNode<K, V,E> temp = neighbors.get(source);
			while(temp != null)
			{
				if(temp.getDest() != null && temp.getDest().compareTo(dest) == 0) 
				{
					existe = temp;
				}

				temp = temp.getSiguiente();
			}
		}

		return existe;  
		
	}
	
	
	
	public int vertexSize() 
	{
		return vertexSize;
	}
	public int edgeSize()
	{
		return edgesSize;
	}
	
	public DirectedGraph<K, V,E> grafoReverso() throws Exception
	{
		DirectedGraph<K, V,E> nuevo = new DirectedGraph<K,V,E>();
		Iterator<EdgeNode<K, V,E>> nuevo2 = neighbors.iterador();
		while(nuevo2.hasNext()) 
		{
			EdgeNode<K, V,E> temp = nuevo2.next();
			nuevo.addVertex(temp.getLlaveActual() , temp.getValor());
		}
		
		Iterator<EdgeNode<K, V,E>> nuevo3 = neighbors.iterador();
		while(nuevo3.hasNext()) 
		{
			EdgeNode<K, V,E> temp = nuevo3.next();
			Iterator<EdgeNode<K, V,E>>	adyacentesTemp = darAdyacentes(temp.llaveActual).iterator();
			while(adyacentesTemp.hasNext()) 
			{
				EdgeNode<K, V,E> temp2 = adyacentesTemp.next();
				nuevo.addEdge( temp2.dest, temp2.llaveActual, temp2.peso);
					
				
			}
			
		}
		
		
		return nuevo;
	}

	public void  addEdge( K source, K dest, double weight ) throws Exception 
	{
		EdgeNode<K, V,E> actual = neighbors.get(source);
		EdgeNode<K, V,E> actual2 = neighbors.get(dest);
		if(neighbors.contiene(source) && neighbors.contiene(dest)) 
		{

			if(!existeEdge(source, dest))
			{
				if(actual.getDest() != null) 
				{
				EdgeNode<K, V,E> nuevo = new EdgeNode<K, V,E>(source, dest, null, weight);
				
				actual.añadirSiguiente(nuevo);
				edgesSize++;
				}
				else 
				{
					actual.setDest(dest);
					actual.setPeso(weight);
					
					edgesSize++;
				}
				if(actual2.getDest() != null) 
				{
				
				EdgeNode<K, V,E> nuevo2 = new EdgeNode<K, V,E>(dest, source, null, weight);
				actual2.añadirSiguiente(nuevo2);
				
				}
				else 
				{
					actual2.setDest(source);
					actual2.setPeso(weight);
					
					
				}
				
			}
		}

	}
	
}
