package api;

import model.data_structures.IList;
import model.logic.VOHoraParadaPorParada;
import model.logic.VOStop;
import model.vo.Camino;
import model.vo.MST;
import model.vo.VOParada;
import model.vo.VOParadaCamino;
import model.vo.VOParadaCongestionada;
import model.vo.VOStopTime;

public interface ISTSManager {

	public IList<Camino> darParadasAlcanzables(int parada, String fecha)throws Exception;

	public IList<IList<VOParada>> darComponentesConexas()throws Exception;

	public void crearSubGrafo(int parada, String horaInicio, String horaFin) throws Exception;

	public IList<model.vo.VOStop> darParadasSubGrafo()throws Exception;

	public IList<model.vo.VOHoraParadaPorParada> darItinerarioLlegada(String stopId)throws Exception;

	public IList<model.vo.VOHoraParadaPorParada> darItinerarioSalida(String stopId)throws Exception;

	public VOParadaCongestionada darParadaMasCongestionada()throws Exception;

	public IList<VOParadaCamino> darCaminoMasCorto(String paradaOrigen, String paradaDestino, String hora)throws Exception;

	public IList<VOParadaCamino> darCaminoMenorTiempo(String paradaOrigen, String paradaDestino, String hora)throws Exception;
	
	public IList<VOParada> darMayorComponenteConexa()throws Exception;

	public IList<VOParadaCamino> darCiclo(String horaInicio)throws Exception;
	
	public void simplificarGrafo()throws Exception;

	public MST darArbolRecubrimientoMinimo(String horaInicio)throws Exception;

	public void ITSInit();

	public void ITScargarGTFS()throws Exception;

	

}
