package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IList;
import model.logic.VOStop;
import model.vo.VOHoraParadaPorParada;
import model.vo.VOParada;
import model.vo.VOParadaCongestionada;


public class STSManagerView {
	
	

		/**
		 * Main
		 *
		 * @param args
		 */
		public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			boolean fin = false;
			while (!fin) {
				printMenu();

				int option = sc.nextInt();

				switch (option) {
				// Cargar
				case 1:

					// Memoria y tiempo
					long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long startTime = System.nanoTime();

					// Cargar data
					Controller.ITScargarGTFS();

					// Tiempo en cargar
					long endTime = System.nanoTime();
					long duration = (endTime - startTime) / (1000000);

					// Memoria usada
					long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "
							+ ((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + " MB");

					break;

				// 1
				case 2:
					// Parada
					System.out.println("Ingrese el id de la parada:");
					String stopId = sc.next();
					// Fecha deseada
					System.out.println(
							"Ingrese la fecha deseada Ej: 20170625 (AnoMesDia) \n Esta fecha se utilizara para los otros metodos.");
					String fechaCase2 = sc.next();

					IList<Camino> paradasAlcanzables = Controller.darParadasAlcanzables(stopId, fechaCase2);
//					TODO
//					for (Camino camino : paradasAlcanzables) {
//						//Obtener todas las paradas e imprimir sus ids
//					}
					System.out.println("Desea conocer un camino? (S/N)");
					String rta = sc.next();
					while(rta.toUpperCase().equals("S")) {
						System.out.println("Conocer camino para llegar a una parada. Ingrese la parada ");
						String stopId2 = sc.next();
						//TODO obtener e imprimir la secuencia de paradas para llegar a stopId2 desde stopId
						
						System.out.println("Desea conocer un camino? (S/N)");
						rta = sc.next();
					}
					
					break;
				// 2
				case 3:

					IList<IList<VOParada>> componentes = Controller.darComponentesConexas();
					int i = 1;
					//TODO Imprimir respuesta de las componentes conexas
					for (IList<VOParada> iList : componentes) {
						System.out.println("Componente " + i++);
						for (VOParada voParada : iList) {
							
						}
					}
					break;

				// 3
				case 4:
					System.out.println("Ingrese el id de la parada origen");
					String stopId4 = sc.next();
					System.out.println("Ingrese la hora de Inicio");
					String horaInicio = sc.next();
					System.out.println("Ingrese la hora de fin");
					String horaFin = sc.next();
					Controller.crearSubGrafo(stopId4, horaInicio, horaFin);
					break;

				// 3A
				case 5:
					IList<VOStop> paradas = Controller.darParadasSubGrafo();
					for (VOStop voParada : paradas) {
						//TODO Imprimir información de las paradas
					}
					break;

				// 3B
				case 6:
					System.out.println("Ingrese el id de la parada a consultar");
					String stopId6 = sc.next();
					IList<VOHoraParadaPorParada> llegadas = Controller.darItinerarioLlegada(stopId6);
					for (VOHoraParadaPorParada voStopTime : llegadas) {
						//TODO
					}
					break;

				// 3C
				case 7:
					System.out.println("Ingrese el id de la parada a consultar");
					String stopId7 = sc.next();
					IList<VOHoraParadaPorParada> salidas = Controller.darItinerarioSalida(stopId7);
					for (VOHoraParadaPorParada voStopTime : salidas) {
						//TODO
					}
					break;

				// 3D
				case 8:
					VOParadaCongestionada paradaCongestionada = Controller.darParadaMasCongestionada();
					System.out.println(paradaCongestionada);
					break;

				// 3E
				case 9:
					System.out.println("Ingrese el id de la parada de origen");
					String paradaO = sc.next();
					
					System.out.println("Ingrese el id de la parada de destino");
					String paradaDestino = sc.next();
					
					System.out.println("Ingrese la hora");
					String hora = sc.next();
					
					IList<VOParadaCamino> camino= Controller.darCaminoMasCorto(paradaO, paradaDestino, hora);
					for (VOParadaCamino voParadaCamino : camino) {
						//TODO Imprimir la informacion del camino
					}
					break;

				// 3F
				case 10:
					System.out.println("Ingrese el id de la parada de origen");
					String paradaOrigen = sc.next();
					
					System.out.println("Ingrese el id de la parada de destino");
					String paradaD = sc.next();
					
					System.out.println("Ingrese la hora");
					String hora3F = sc.next();
					
					IList<VOParadaCamino> camino3F= Controller.darCaminoMenorTiempo(paradaOrigen, paradaD, hora3F);
					for (VOParadaCamino voParadaCamino : camino3F) {
						//TODO Imprimir la informacion del camino
					}
					break;
				// 3G
				case 11:

					IList<VOParada> cc = Controller.darMayorComponenteConexa();
					for (VOParada voParada : cc) {
						//Imprimir informacion
					}

					break;

				// 3H
				case 12:
					System.out.println("Ingrese la hora de inicio para la búsqueda");
					String horaInicio12 = sc.next();
					
					IList<VOParadaCamino> ciclo = Controller.darCiclo(horaInicio12);
					//TODO Imprimir la información de acuerdo al enunciado
					break;

	            // 3I
				case 13:
					System.out.println("Ingrese la hora de inicio para la búsqueda");
					String horaInicio13 = sc.next();
					MST arbol = Controller.darArbolRecubrimientoMinimo(horaInicio13);
					//TODO Imprimir la información de acuerdo al enunciado
					break;
				// SALIR
				case 14:

					fin = true;
					sc.close();
					break;

				}
			}
		}

		/**
		 * Menu
		 */

		private static void printMenu() {
			System.out.println("---------ISIS 1206 - Estructuras de datos----------");
			System.out.println("---------------------Proyecto 3----------------------");
			System.out.println("1. Cargar la información estática necesaria para la operación del sistema");
			System.out.println("2. Dar las paradas alcanzables desde una parada");
			System.out.println(
					"3. Dar componentes fuertemente conexas en el grafo de paradas");
			System.out.println(
					"4. Crear un subgrafo a partir de una fecha, parada origen y rango de hora dadas");
			System.out.println(
					"5. Dar las paradas del subgrafo");
			System.out.println("6. Dar el itinerario de llegada de una parada");
		System.out.println(
					"7.Dar el itinerario de salida de una parada");
		System.out.println(
					"8. Dar la parada más congestionada (con más viajes de llegada y de salida) del subgrafo");
			System.out.println(
					"9. Dar el camino más corto (de menor distancia) entre dos paradas");
			System.out.println(
					"10. Dar el camino de menor tiempo entre dos paradas");
			System.out.println(
					"11. Dar la mayor componente conexa del subgrafo");
			System.out.println("12. Dar un ciclo simple del subgrafo");
			System.out.println(
					"13. Dar el árbol de recubrimiento mínimo del grafo simplicado");
			System.out.println("14. Salir.\n");
			System.out.println("Ingrese la opción deseada y luego presione enter: (e.g., 1):");

		}
	}